import { Component, OnInit } from '@angular/core';
import {Post} from '../../models/post-interface';
import {Pagination} from '../../models/pagination.interface';
import {ApiService} from '../../services/api.service';
import {PaginationService} from '../../services/pagination.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  private allItems: Array<Post>;
  pager: Pagination;
  pagedItems: Array<Post>;

  constructor(
    private router: Router,
    private httpService: ApiService,
    private paginationService: PaginationService
  ) {}

  ngOnInit() {
    this.httpService.getPosts().subscribe(data => {
      this.allItems = data;
      this.setPage(1);
    });
  }

  setPage(page: number) {
    this.pager = this.paginationService.getPager(this.allItems.length, page, 9);
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
}
