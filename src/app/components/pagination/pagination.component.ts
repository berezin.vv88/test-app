import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import {Pagination} from '../../models/pagination.interface';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Input() pager: Pagination;
  @Output() setPageTo = new EventEmitter<number>();
  setPage(page) {
    this.setPageTo.emit(page);
  }

  constructor() {
  }

  ngOnInit() {
  }

}
